#!/bin/sh
if [[ "$OSTYPE" == "darwin"* ]]; then
    alias box="cd /Users/coleglotfelty/Library/CloudStorage/Box-Box/"
    alias sshfs-halligan="sshfs cglotf01@homework.cs.tufts.edu:/h/cglotf01/ ~/Halligan/ -o reconnect,IdentityFile=/Users/coleglotfelty/.ssh/halligan"
    alias unmount-halligan="diskutil umount force ~/Halligan/"
fi
