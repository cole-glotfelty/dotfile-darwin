#!/bin/sh

HISTSIZE=1000000
SAVEHIST=1000000
export EDITOR="nvim"
export PATH="$HOME/.local/bin":$PATH
export MANWIDTH=999
export PATH=$HOME/.cargo/bin:$PATH
export PATH=$HOME/.local/share/go/bin:$PATH
export PATH="$HOME/.local/share/neovim/bin":$PATH
export PATH=$HOME/.emacs.d/bin/:$PATH
export PATH="/opt/homebrew/opt/python@3.11/libexec/bin":$PATH
export PATH="$PATH":"$HOME/.local/bin/scripts/"
