#!bin/sh
[ -f "/Users/coleglotfelty/.ghcup/env" ] && source "/Users/coleglotfelty/.ghcup/env" # ghcup-env

# Inspired by https://github.com/ChristianChiarulli/Machfiles/tree/master/zsh
[ -f "$HOME/.local/share/zap/zap.zsh" ] && source "$HOME/.local/share/zap/zap.zsh"

# history
HISTFILE=~/.zsh_history

# source
plug "$HOME/.config/zsh/aliases.zsh"
plug "$HOME/.config/zsh/exports.zsh"

# enable colors
autoload -U colors && colors

# auto/tab completion
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)

# plugins
plug "zsh-users/zsh-autosuggestions"
plug "zap-zsh/supercharge"
plug "zap-zsh/atmachine-prompt"
plug "zap-zsh/exa"
plug "zsh-users/zsh-syntax-highlighting"

# keybinds
bindkey '^ ' autosuggest-accept

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/Users/coleglotfelty/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/Users/coleglotfelty/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/Users/coleglotfelty/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/Users/coleglotfelty/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
